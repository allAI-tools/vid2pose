module.exports = {
  "cmds": {
    "nvidia": "pip install torch torchvision torchaudio xformers --index-url https://download.pytorch.org/whl/cu118",
    "amd": "pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/rocm5.6",
    "default": "pip install torch torchvision torchaudio"
  },
  "requires": [{
    "type": "conda",
    "name": "ffmpeg",
    "args": "-c conda-forge"
  }, {
    "gpu": "nvidia",
    "name": "cuda",
  }],
  "run": [{
    "method": "shell.run",
    "params": { "message": "git clone https://mirror.ghproxy.com/https://github.com/candywrap/vid2pose app" }
  }, {
    "method": "shell.run",
    "params": {
      "venv": "env",
      "env": {
        "PYTHONNOUSERSITE": "True",
        "PIP_INDEX_URL": "https://mirrors.aliyun.com/pypi/simple",
        "HF_ENDPOINT": "https://hf-mirror.com",
      },
      "path": "app",
      "message": [
        "{{(gpu === 'nvidia' ? self.cmds.nvidia : (gpu === 'amd' ? self.cmds.amd : self.cmds.default))}}",
        "pip install opencv-python gradio==3.50.2",
        "pip install -U openmim",
        "mim install mmengine mmcv mmdet mmpose",
        "pip install -r requirements.txt"
      ]
    }
  }, {
    "method": "input",
    "params": { "title": "Install Success", "description": "Go back to the dashboard and launch the app!" }
  }, {
    "method": "notify",
    "params": {
      "html": "安装完成，请点击开始按钮运行项目！"
    }
  }]
}
